// use the "require" directive to load node,js modules
// a "module" is a software component/part of a program that contains one or more routines
// "http" module will let node.js transfer a data using the hyper text transfer protocol

let http = require("http");

http.createServer(function (request,response) {

	// use the writeHead() method to:
	// set a status code for the response
	// set the content-type of the response as a plain text message
	response.writeHead(200, {'Content-Type': 'text/plain'});
	response.end("Hello, 8245!");

}).listen(4000);

// when server is running, console will print the message
console.log('Server running at localHost:4000');