const http = require("http");

const port = 4000;

const server = http.createServer((request, response) => {

	// accessing the "greeting" route returns a message of "Hello World"

	if(request.url == '/greeting') {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Hello World");
	} else if(request.url == '/homepage') {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Welcome to our homepage");
	} else {

		// set a status code for the response - a 404 means "NOT FOUND"
		response.writeHead(404, {'Content-Type': 'text/plain'});
		response.end('Page not Available');
	}

});

server.listen(port);

console.log(`Server now accessible at localhost: ${port}`);